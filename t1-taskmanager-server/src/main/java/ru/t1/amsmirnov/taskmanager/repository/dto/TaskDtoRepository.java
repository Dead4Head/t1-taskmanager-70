package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

import java.util.List;

public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull final String projectId);

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    void deleteByUserIdAndProjectId(final @NotNull String userId, final @NotNull String projectId);

    void deleteByProjectId(final @NotNull String projectId);

}
