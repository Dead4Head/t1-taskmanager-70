//package ru.t1.amsmirnov.taskmanager.repository.listener;
//
//import org.apache.log4j.Logger;
//import org.hibernate.event.spi.*;
//import org.hibernate.persister.entity.EntityPersister;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import ru.t1.amsmirnov.taskmanager.dto.log.OperationEvent;
//import ru.t1.amsmirnov.taskmanager.enumerated.log.OperationType;
//
//public final class EntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {
//
//    @NotNull
//    private final static Logger logger = Logger.getLogger(EntityListener.class);
//
//    @Nullable
//    private JmsLoggerProducer jmsLoggerProducer;
//
//    public EntityListener() {
//    }
//
//    public EntityListener(@NotNull final JmsLoggerProducer messageExecutor) {
//        this.jmsLoggerProducer = messageExecutor;
//    }
//
//    @Override
//    public void onPostDelete(@NotNull final PostDeleteEvent event) {
//        sendMessage(event.getEntity(), OperationType.DELETE);
//    }
//
//    @Override
//    public void onPostInsert(@NotNull final PostInsertEvent event) {
//        sendMessage(event.getEntity(), OperationType.INSERT);
//    }
//
//    @Override
//    public void onPostUpdate(@NotNull final PostUpdateEvent event) {
//        sendMessage(event.getEntity(), OperationType.UPDATE);
//    }
//
//    @Override
//    public boolean requiresPostCommitHanding(@NotNull final EntityPersister persister) {
//        return false;
//    }
//
//    public void sendMessage(
//            @NotNull final Object entity,
//            @NotNull final OperationType type
//    ) {
//        if (jmsLoggerProducer == null) return;
//        try {
//            jmsLoggerProducer.sendMessage(new OperationEvent(entity, type));
//        } catch (final Exception e) {
//            logger.error(e);
//        }
//    }
//
//}
