package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.t1.amsmirnov.taskmanager.api.IProjectEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;
import ru.t1.amsmirnov.taskmanager.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping(value = "/api/projects", produces = "application/json")
@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @Autowired
    private ProjectDtoService projectService;

    @Nullable
    @Override
    @WebMethod
    @PostMapping("/create")
    public ProjectWebDto create(
            @WebParam(name = "name", partName = "name") @RequestParam("name") String name,
            @WebParam(name = "description", partName = "description") @RequestParam(value = "description", defaultValue = "") String desc
    ) {
        try {
            return projectService.create(UserUtil.getUserId(), name, desc);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectWebDto> findAll() {
        try {
            return projectService.findAll(UserUtil.getUserId());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectWebDto findById(
            @WebParam(name = "id", partName = "id") @PathVariable("id") final String id
    ) {
        try {
            return projectService.findOneById(UserUtil.getUserId(), id);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectWebDto save(
            @WebParam(name = "project", partName = "project") @RequestBody final ProjectWebDto project
    ) {
        try {
            return projectService.update(UserUtil.getUserId(), project);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public ProjectWebDto delete(
            @WebParam(name = "project", partName = "project") @RequestBody final ProjectWebDto project
    ) {
        try {
            return projectService.removeOne(UserUtil.getUserId(), project);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @DeleteMapping("/delete/{id}")
    @WebMethod(operationName = "deleteById")
    public ProjectWebDto delete(
            @WebParam(name = "id", partName = "id") @PathVariable("id") final String id
    ) {
        try {
            return projectService.removeOneById(UserUtil.getUserId(), id);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        try {
            projectService.removeAll(UserUtil.getUserId());
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
