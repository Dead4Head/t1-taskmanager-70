package ru.t1.amsmirnov.taskmanager.controller.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.dto.CustomUser;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;

import java.util.HashMap;
import java.util.Map;

@Controller
public class TasksController {

    @Autowired
    private TaskDtoService taskService;

    @Autowired
    private ProjectDtoService projectService;

    @GetMapping("/tasks")
    public ModelAndView tasks(
            @AuthenticationPrincipal final CustomUser user
    ) {
        try {
            final Map<String, ?> model = new HashMap<String, Object>() {{
                put("tasks", taskService.findAll(user.getUserId()));
                put("projectService", projectService);
                put("userId", user.getUserId());
            }};

            return new ModelAndView("task-list", model);
        } catch (Exception e) {
            final ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("error");
            modelAndView.addObject("error", e);
            return modelAndView;
        }
    }

}
