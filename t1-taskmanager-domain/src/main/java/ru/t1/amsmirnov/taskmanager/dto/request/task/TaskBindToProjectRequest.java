package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskBindToProjectRequest() {
    }

    public TaskBindToProjectRequest(
            @Nullable final String token,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        super(token);
        setTaskId(taskId);
        setProjectId(projectId);
    }

    @Nullable
    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(@Nullable final String taskId) {
        this.taskId = taskId;
    }

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}